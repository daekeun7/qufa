- **QuFa** (Data **Qu**ality and **Fa**irness)

- 데이터 품질 평가기반 데이터 고도화 및 데이터셋 보정 기술 개발 (Development of data improvement and dataset correction technology based on data quality assessment)

- 2020 정보통신·방송 연구개발 사업 (SW컴퓨팅산업원천기술개발)

- Organization

| 구분 | 기관명 |
| ------ | ------ |
| 전담기관 | 정보통신기획평가원 (IITP) |
| 주관기관 | 부산대학교 |
| 공동연구기관 | 서울대학교, 서울시립대학교, 이화여자대학교, 한성대학교, (주)프람트테크놀로지, (주)아이소프트 |
| 수요기업 | (주)프람트테크놀로지, (주)아이소프트 |

<ul>
    <li>
      <label>QuFa (Repository - structure of directories)</label>
      <ul>
        <li>
          <label>dataDomain (데이터 셋)</label>
          <ul>
            <li>
                <label>traffic (교통 데이터/부산대학교) - <a href="https://gitlab.com/qufa/qufa/-/tree/master/dataDomain/traffic/dataSet">data set</a></label>
            </li>
            <li>
                <label>health (보건 데이터/서울대학교) - <a href="https://gitlab.com/qufa/qufa/-/tree/master/dataDomain/health/dataSet">data set</a></label>
            </li>
            <li>
                <label>culturalTourism (문화관광 데이터/서울시립대학교) - <a href="https://gitlab.com/qufa/qufa/-/tree/master/dataDomain/culturalTourism/dataSet">data set</a></label>
            </li>
            <li>
                <label>environment (환경 데이터/이화여자대학교) - <a href="https://gitlab.com/qufa/qufa/-/tree/master/dataDomain/environment/dataSet">data set</a></label>
            </li>
            <li>
                <label>disasterSafety (재난안전 데이터/한성대학교) - <a href="https://gitlab.com/qufa/qufa/-/tree/master/dataDomain/disasterSafety/dataSet">data set</a></label>
            </li>
          </ul>
        </li><br>
        <li>
          <label>dataQuality (데이터 품질팀)</label>
          <ul>
            <li>
                <label>uos (서울시립대학교) - source code</label>
            </li>
            <li>
                <label>ewu (이화여자대학교) - source code</label>
            </li>
            <li>
                <label>hsu (한성대학교) - source code</label>
            </li>
          </ul>
        </li><br>
        <li>
          <label>dataFairness (데이터 공정성팀)</label>
          <ul>
            <li>
                <label>pnu (부산대학교) - <a href=https://gitlab.com/qufa/qufa/-/tree/master/dataFairness/pnu>source code</a></label>
            </li>
            <li>
                <label>snu (서울대학교) - source code</label>
            </li>
          </ul>
        </li><br>
        <li>
          <label>framework (상용화팀/수요기업)</label>
          <ul>
            <li>
                <label>systemIntegration ((주)프람트테크놀로지) - source code</label>
            </li>
            <li>
                <label>visualization ((주)아이소프트) - source code</label>
            </li>
          </ul>
        </li>
      </ul>
    </li>
</ul>
